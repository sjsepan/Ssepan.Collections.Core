﻿using System;
using System.Collections.Generic;
using Ssepan.Utility.Core;
using System.Reflection;

namespace Ssepan.Collections.Core
{
	public class KeyedStringComparer : IComparer<string>
    {
        // Create a field for the desired sort option
        private readonly int _keyIndex;
        private readonly int _keyLength = -1;
        private readonly bool _descending;

        /// <summary>
        /// Create a constructor that uses the default sort key instantiated in the comparer
        /// </summary>
        public KeyedStringComparer()
        {
        }
        /// <summary>
        /// Create a constructor that uses the default sort key instantiated in the comparer, and does a reverseable sort
        /// </summary>
        /// <param name="descending"></param>
        public KeyedStringComparer(bool descending)
        {
            _descending = descending;
        }
        /// <summary>
        /// Create a constructor that uses the passed sort key for the comparer
        /// </summary>
        /// <param name="keyIndex"></param>
        /// <param name="keyLength"></param>
        public KeyedStringComparer(int keyIndex, int keyLength)
        {
            _keyIndex = keyIndex;
            _keyLength = keyLength;
        }
        /// <summary>
        /// Create a constructor that uses the passed sort key for the comparer, and does a reverseable sort
        /// </summary>
        /// <param name="keyIndex"></param>
        /// <param name="keyLength"></param>
        /// <param name="descending"></param>
        public KeyedStringComparer(int keyIndex, int keyLength, bool descending)
        {
            _keyIndex = keyIndex;
            _keyLength = keyLength;
            _descending = descending;
        }
        #region IComparer<String> Members

        public int Compare(string x, string y)
        {
			int returnValue = default;
            try
            {
                if (_keyLength == -1)
                {
                    //TODO:implement default string comparison
                    throw new ArgumentException(string.Format("Invalid KeyLength: '{0}'\r\nDefault string comparison not implemented.", _keyLength));
                }
                else if (_keyLength > 0)
                {
                    returnValue = x.Substring(_keyIndex, _keyLength).CompareTo(y.Substring(_keyIndex, _keyLength));
                }
                else
                {
                    throw new ArgumentException(string.Format("Invalid KeyLength: '{0}'", _keyLength));
                }

                if (_descending)
                {
					returnValue *= -1;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            return returnValue;
        }

        #endregion
    }
}
