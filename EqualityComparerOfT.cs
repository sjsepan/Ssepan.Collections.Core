﻿using System;
using System.Collections.Generic;

namespace Ssepan.Collections.Core
{
	/// <summary>
	/// Comparer wrapper from lambda that can be used where only delegates are accepted (i.e. .Except(), etc.)
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class EqualityComparerOfT<T>: IEqualityComparer<T>
    {
        private readonly Func<T, T, bool> _comparer;

        public EqualityComparerOfT(Func<T, T, bool> comparer)
        {
			ArgumentNullException.ThrowIfNull(comparer);

			_comparer = comparer;
        }

        public bool Equals(T x, T y)
        {
            return _comparer(x, y);
        }

        public int GetHashCode(T obj)
        {
            return obj.ToString().ToLower().GetHashCode();
        }
    }
}
