﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Ssepan.Utility.Core;
using System.Reflection;

namespace Ssepan.Collections.Core
{
	/// <summary>
	/// Extension to List(Of T) />
	/// </summary>
	public static class ListOfTExtension
    {
        public enum ShiftTypes
        {
            Promote,
            Demote
        }

        #region BindingList<T>
        /// <summary>
        /// Shift TItem item in TList list
        /// </summary>
        /// <typeparam name="TList"> </typeparam>
        /// <typeparam name="TItem"></typeparam>
        /// <param name="items"></param>
        /// <param name="shiftType"></param>
        /// <param name="itemMatch"></param>
        /// <param name="swapItemMatch"></param>
        public static void ShiftListItem<TList, TItem>
        (
            this TList items,
            ShiftTypes shiftType,
            Predicate<TItem> itemMatch,
            Func<TItem, TItem, bool> swapItemMatch
        )
            where TList : BindingList<TItem>
        {
            try
            {
				int searchOffset = default;
				int searchLimit = default;
				bool isFoundSwapItem = default;
				bool isAtEnd = default;
                TItem item = default;
                TItem tempItem = default;
				int itemIndex = default;
				int swapItemIndex = default;

                //define direction
                if (shiftType == ShiftTypes.Promote)
                {
                    searchOffset = -1;
                    searchLimit = 0;
                }
                else if (shiftType == ShiftTypes.Demote)
                {
                    searchOffset = 1;
                    searchLimit = items.Count - 1;
                }

                //find index with which to swap
                item = items.Find(itemMatch); //item => item.Property == propertyValue
                itemIndex = items.IndexOf(item);
                swapItemIndex = itemIndex;
                isAtEnd = itemIndex == searchLimit;
                while (!isFoundSwapItem && !isAtEnd)
                {
                    swapItemIndex += searchOffset;

                    isFoundSwapItem = swapItemMatch(item, items[swapItemIndex]); //evaluate lambda, or true (to swap with any item, unconditionally)
                    isAtEnd = swapItemIndex == searchLimit;
                }

                //swap
                if (isFoundSwapItem)
                {
                    tempItem = items[swapItemIndex];
                    items[swapItemIndex] = items[itemIndex];
                    items[itemIndex] = tempItem;
                    tempItem = default;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }
        #endregion BindingList<T>

        #region EquatableList<T>
        /// <summary>
        /// Return EquatableList Of T from supplied List Of T.
        /// </summary>
        public static EquatableList<T> ToEquatableList<T>(this List<T> list)
            where T : IEquatable<T>
		{
            return [.. list];
		}

        /// <summary>
        /// Return EquatableList Of T from supplied IEnumerable Of T.
        /// </summary>
        public static EquatableList<T> ToEquatableList<T>(this IEnumerable<T> list)
            where T : IEquatable<T>
		{
            return [.. list];
        }
		#endregion EquatableList<T>

		#region EquatableBindingList<T>
		/// <summary>
		/// Return EquatableBindingList Of T from supplied List Of T.
		/// </summary>
		public static EquatableBindingList<T> ToEquatableBindingList<T>(this List<T> list)
			where T : IEquatable<T>
		{
			return [.. list];
		}

        /// <summary>
        /// Return EquatableBindingList Of T from supplied IEnumerable Of T.
        /// </summary>
        public static EquatableBindingList<T> ToEquatableBindingList<T>(this IEnumerable<T> list)
            where T : IEquatable<T>
		{
			return [.. list];
		}
        #endregion EquatableBindingList<T>

        #region OrderedEquatableList<T>
        /// <summary>
        /// Return OrderedEquatableList Of T from supplied List Of T.
        /// </summary>
        public static OrderedEquatableList<T> ToOrderedEquatableList<T>(this List<T> list)
            where T : IEquatable<T>
		{
            return [.. list];
		}

        /// <summary>
        /// Return OrderedEquatableList Of T from supplied IEnumerable Of T.
        /// </summary>
        public static OrderedEquatableList<T> ToOrderedEquatableList<T>(this IEnumerable<T> list)
            where T : IEquatable<T>
		{
            return [.. list];
        }
        #endregion OrderedEquatableList<T>

        #region OrderedEquatableBindingList<T>
        /// <summary>
        /// Return OrderedEquatableBindingList Of T from supplied List Of T.
        /// </summary>
        public static OrderedEquatableBindingList<T> ToOrderedEquatableBindingList<T>(this List<T> list)
            where T : IEquatable<T>
		{
            return [.. list];
		}

        /// <summary>
        /// Return OrderedEquatableBindingList Of T from supplied IEnumerable Of T.
        /// </summary>
        public static OrderedEquatableBindingList<T> ToOrderedEquatableBindingList<T>(this IEnumerable<T> list)
            where T : IEquatable<T>
		{
            return [.. list];
        }
        #endregion OrderedEquatableBindingList<T>
    }
}