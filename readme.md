# readme.md - README for Ssepan.Collections.Core

## About

Common library of Collections functions for C# .Net Core applications; requires ssepan.utility.core

### Purpose

To encapsulate common Collections functionality, reduce custom coding needed to start a new project, and provide consistency across projects.

### Usage notes

~...

### Setup

### History

6.0:
~initial build on .Net (Core)
~update to net8.0
~refactor to newer C# features
~refactor to existing language types
~perform format linting

2.8:
~initial build on .Net Framework

Steve Sepan
<sjsepan@yahoo.com>
2/8/2024
