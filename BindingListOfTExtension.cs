﻿using System;
using System.ComponentModel;
using System.Linq;
using Ssepan.Utility.Core;
using System.Reflection;

namespace Ssepan.Collections.Core
{
	/// <summary>
	/// Extension to BindingList.
	/// Implements Find(Of T).
	/// </summary>
	[Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public static class BindingListOfTExtension
    {
        #region BindingList Members

        /// <summary>
        /// Implements missing Find(Predicate(Of T)), like List(Of T).Find(Predicate(Of T))
        /// </summary>
        /// <param name="list"></param>
        /// <param name="match"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns>T</returns>
        public static T Find<T>(this BindingList<T> list, Predicate<T> match)
        {
            T returnValue = default;
            try
            {   //TODO: implement FindCore instead?
                returnValue = list.ToList().Find(match);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            return returnValue;
        }

        #endregion BindingList<T>
    }
}
