﻿using System;
using System.ComponentModel;
using Ssepan.Utility.Core;
using System.Reflection;

namespace Ssepan.Collections.Core
{
	/// <summary>
	/// Subclass of BindingList Of T that implements IEquatable, and contains items that implement IEquatable.
	/// This class checks the content and arrangement of the list.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	[Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class OrderedEquatableBindingList<T> :
        BindingList<T>,                         //start with BindingList Of T
        IEquatable<OrderedEquatableBindingList<T>>     //implements IEquatable
        where T : IEquatable<T>
    {
        #region IEquatable<T> Members

        /// <summary>
        /// Test for equality of contents *and arrangement* of this list versus another OrderedEquatableBindingList Of T.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(OrderedEquatableBindingList<T> other)
        {
            bool returnValue = true;
			try
			{
                if (Count != other.Count)
                {
                    //different number of items
                    returnValue = false;
                }
                else
                {
                    foreach (T item in this)
					{
						int thisIndex = IndexOf(item);
						int otherIndex = other.IndexOf(item);
						if (thisIndex != otherIndex)
                        {
                            //items missing or in different positions
                            returnValue = false;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                returnValue = false;
            }
            return returnValue;
        }

        #endregion
    }
}
